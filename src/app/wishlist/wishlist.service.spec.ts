import { TestBed, inject } from '@angular/core/testing';

import { WishlistService } from './wishlist.service';
import {Product} from '../product/product.interface';



describe('WishlistService', () => {
  let service: WishlistService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WishlistService]
    });

    service = TestBed.get(WishlistService);

    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };

    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem')
      .and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear')
      .and.callFake(mockLocalStorage.clear);
  });

  /*it('should be created', inject([WishlistService], (service: WishlistService) => {
    expect(service).toBeTruthy();
  }));*/

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get an empty list if nothing is set', () => {
    expect(service.get().products.length).toEqual(0);
  });

  /**
   * Checks change name process
   */
  it('should change names', () => {
    const newName = 'Test Wishlist';
    expect(service.getName()).toBe('My Wishlist');
    service.setName(newName);
    expect(service.getName()).toBe(newName);
    expect(service.get().name).toBe(newName);
  });

  /**
   * Tests product live-cycle: add -> list -> delete -> list
   */
  it('should have a product live-cicly', () => {
    const product: Product = {
      name: 'Product Name',
      brand: 'Brand',
      description: 'One simple description',
      url: 'http://google.de',
      code: '12345',
      price: {
        formattedValue: '€ 0.00'
      }
    };
    expect(service.get().products.length).toEqual(0);
    expect(service.add(product)).toBeTruthy();
    expect(service.check(product)).toBeTruthy();
    expect(service.get().products.length).toBeGreaterThan(0);
    expect(service.delete(product)).toBeTruthy();
    expect(service.get().products.length).toEqual(0);
  });
});
