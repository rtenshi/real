import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ProductModule } from '../product/product.module';
import { WishlistListComponent } from './wishlist-list/wishlist-list.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ProductModule,
    FormsModule
  ],
  exports: [
    WishlistListComponent
  ],
  declarations: [WishlistListComponent]
})
export class WishlistModule { }
