import {Component, Input, OnInit} from '@angular/core';
import {WishlistService} from '../wishlist.service';
import {Product} from '../../product/product.interface';

@Component({
  selector: 'app-wishlist-list',
  templateUrl: './wishlist-list.component.html',
  styleUrls: ['./wishlist-list.component.css'],
  providers: [WishlistService]
})
export class WishlistListComponent implements OnInit {
  @Input() wishlistName = 'My Wishlist';

  public edit = false;
  public name: string;
  public products: Product[];

  constructor(private _store: WishlistService) {}

  ngOnInit() {
    this.loadData();
  }

  /**
   * Functional Javascript to Update Data on Component
   */
  private loadData() {
    try {
      const tmp = this._store.get();
      this.wishlistName = tmp.name;
      this.name = tmp.name;
      this.products = tmp.products;
    } catch (err) {
      console.warn(err);
    }
  }

  /**
   * Remote Item from the List
   * @param {Product} prod
   * @returns {boolean}
   */
  removeItem(prod: Product) {
    if (!prod) {
      return false;
    }
    this.loadData();
  }

  /**
   * Save a new name for the Wishlist
   * @param {string} newName
   * @returns {boolean}
   */
  saveName() {
    if (!this.wishlistName.length) {
      return false;
    }
    this.name = this.wishlistName;            // save new name on display
    this.edit = false;              // close edit position
    this._store.setName(this.wishlistName);   // save new name on localstorage
    this.loadData();
    return true;
  }

  /**
   * Cancel the edit and reset the input inside the editor.
   * @returns {boolean}
   */
  cancelEdit() {
    this.wishlistName = this.name;
    this.edit = false;
    return true;
  }

  /**
   * Toggle Editor
   * @returns {boolean}
   */
  toggle() {
    return this.edit = !this.edit;
  }
}
