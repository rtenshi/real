import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WishlistListComponent } from './wishlist-list.component';
import {FormsModule} from '@angular/forms';
import {SingleProductComponent} from '../../product/single-product/single-product.component';

describe('WishlistListComponent', () => {
  let component: WishlistListComponent;
  let fixture: ComponentFixture<WishlistListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        WishlistListComponent,
        SingleProductComponent
      ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WishlistListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
