import { Injectable } from '@angular/core';
import { Product} from '../product/product.interface';

/**
 * Localstorage Wishlist
 */
@Injectable()
export class WishlistService {

  private key = 'myWishlistRL';

  constructor() { }

  /**
   * get the Wishlist from localStorage
   *
   * @returns {Array<Product>}
   */
  private getWishlist(): Array<Product> {
    const list = JSON.parse(localStorage.getItem(`${this.key}_prod`));
    if (list === null) {
      return [];
    } else {
      return list;
    }
  }

  /**
   * save the Wishlist at localStorage
   *
   * @param {Product[]} arr
   */
  private saveWishlist(arr: Product[]) {
    localStorage.setItem(`${this.key}_prod`, JSON.stringify(arr));
  }

  /**
   * get name from the wishlist
   *
   * @returns {string | null}
   */
  public getName() {
    if (!localStorage.getItem(`${this.key}_name`)) {
      return 'My Wishlist';
    }
    return localStorage.getItem(`${this.key}_name`);
  }

  /**
   * set name from the wishlist
   *
   * @param {string} name
   */
  public setName(name: string) {
    localStorage.setItem(`${this.key}_name`, name);
  }

  /**
   * add Product to the Wishlist
   *
   * @param {Product} product
   */
  public add(product: Product) {
    if (this.check(product)) {
      return false;
    }
    const arr = this.getWishlist();
    arr.push(product);
    this.saveWishlist(arr);
    return true;
  }

  /**
   * remove a product from the Wishlist
   *
   * @param {Product} product
   */
  public delete(product: Product) {
    let arr = this.getWishlist();
    arr = arr.filter(x => x.code !== product.code);
    this.saveWishlist(arr);
    return true;
  }

  /**
   * Public call to get the Wishlist
   *
   * @returns {Product[]}
   */
  public get()  {
    return {
      products: this.getWishlist(),
      name: this.getName(),
    };
  }

  /**
   * Check if Product is saved on the Wishlist
   * @param {Product} prod
   * @returns {boolean}
   */
  public check(prod: Product) {
    return (this.getWishlist().filter(x => x.code === prod.code).length > 0);
  }
}
