import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {WishlistModule} from './wishlist/wishlist.module';
import {ProductModule} from './product/product.module';
import { AppRoutingModule } from './/app-routing.module';
import {ProductService} from './product/product.service';
import {HttpClientModule} from '@angular/common/http';
import {WishlistService} from './wishlist/wishlist.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ProductModule,
    RouterModule,
    WishlistModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [HttpClientModule, ProductService, WishlistService],
  bootstrap: [AppComponent]
})


export class AppModule { }
