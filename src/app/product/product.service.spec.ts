import {TestBed, inject, async} from '@angular/core/testing';

import { ProductService } from './product.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Product} from './product.interface';

describe('ProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService]
    });
  });

  it('should be created', inject([ProductService], (service: ProductService) => {
    expect(service).toBeTruthy();
  }));

  it('should get a product list', async(inject([ProductService], (service: ProductService) => {
    service.getProducts('pizza', 'relevance').subscribe(res => {
      expect(res['products'].length).toBeGreaterThan(0);
    });
  })));

  it('should get the next page o the product list', async(inject([ProductService], (service: ProductService) => {
    const pagination = {
      currentPage: 0,
      pageSize: 20,
      sort: 'relevance',
      totalPages: 1,
      totalResults: 0,
    };
    service.getNext('pizza', pagination).subscribe(res => {
      expect(res['products'].length).toBeGreaterThan(0);
      expect(res['pagination'].currentPage).toBe('1');
    });
  })));

  it('should get the next page o the product list', async(inject([ProductService], (service: ProductService) => {
    const pagination = {
      currentPage: 5,
      pageSize: 20,
      sort: 'relevance',
      totalPages: 1,
      totalResults: 0,
    };

    service.getNext('pizza', pagination).subscribe(res => {
      expect(res['products'].length).toBeGreaterThan(0);
      expect(res['pagination'].currentPage).toBe(4);
    });
  })));



});
