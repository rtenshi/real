import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductComponent } from './list-product.component';
import {SingleProductComponent} from '../single-product/single-product.component';
import {ProductService} from '../product.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppComponent} from '../../app.component';

describe('ListProductComponent', () => {
  let component: ListProductComponent;
  let fixture: ComponentFixture<ListProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListProductComponent,
        SingleProductComponent
      ],
      imports: [HttpClientTestingModule],
      providers: [ProductService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
