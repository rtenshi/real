import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ProductService} from '../product.service';
import { Product} from '../product.interface';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css'],
  providers: [ProductService]
})
export class ListProductComponent implements OnChanges {
  @Input() search: string;
  public currentQuery: string;
  public nav = false;
  public response;
  public products: Product[] = [];
  public pagination = {
    currentPage: 0,
    pageSize: 20,
    sort: 'relevance',
    totalPages: 1,
    totalResults: 0,
  };

  constructor(private _products: ProductService) {}

  /**
   * Reset the search data
   * @returns {{}}
   */
  private resetData() {
    this.nav = false;
    this.currentQuery = '';
    this.products = [];
    this.pagination = {
      currentPage: 0,
      pageSize: 20,
      sort: 'relevance',
      totalPages: 1,
      totalResults: 0,
    };
    return this.response = {};
  }

  /**
   * Update the search Data on the component
   * @param res
   * @returns {any}
   */
  private updateData(res) {
    this.nav = true;
    this.currentQuery = res['currentQuery'];
    this.products = res['products'];
    this.pagination = res['pagination'];
    return this.response = res;
  }

  /**
   * Update the search parameter on the product list, if changed on parent
   * @param {SimpleChanges} change
   */
  ngOnChanges(change: SimpleChanges) {
    if (typeof change['search'] !== 'undefined') {
      this.getProduct(change.search.currentValue);
    }
  }

  /**
   * Load standard request
   * @param val
   * @returns {boolean}
   */
  getProduct(val) {
    if (typeof val === 'undefined') {
      return false;
    }
    if (val.length < 3) {
      return false;
    }
    this._products.getProducts(val, 'relevance')
      .subscribe(res => this.updateData(res));
  }

  /**
   * Load next page using the setted parameters
   * @returns {boolean}
   */
  goToNext() {
    if (this.pagination.currentPage >= this.pagination.totalPages) {
      return false;
    }
    this._products.getNext(this.response.freeTextSearch, this.pagination)
      .subscribe(res => this.updateData(res));
  }

  /**
   * Load previous page using setted parameters
   * @returns {boolean}
   */
  goToPrevious() {
    if (this.pagination.currentPage < 0) {
      return false;
    }
    this._products.getPrevious(this.response.freeTextSearch, this.pagination)
      .subscribe(res => this.updateData(res));
  }
}
