import {Component} from '@angular/core';

/**
 * Search Component
 */

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  public searchString: string;

  constructor() {}
}
