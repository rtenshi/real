import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

const url = 'https://api.efood.real.de/api/v2/real/products/search?';

@Injectable()
export class ProductService {

  constructor(private http: HttpClient) { }

  /**
   * Make a single request
   * @param {string} query
   * @param {string} sort
   * @returns {Observable<Object>}
   */
  getProducts(query: string, sort: string) {
    return this.http.get(url + 'query=' + query + ':' + sort + '&pageSize=12');
  }

  /**
   * get the next page of the search
   * @param {string} query
   * @param pagination
   * @returns {Observable<Object>}
   */
  getNext(query: string, pagination) {
    const next = pagination.currentPage++;
    return this.http.get(url + `query=${query}:${pagination.sort}&currentPage=${pagination.currentPage}&pageSize=${pagination.pageSize}`);
  }

  /**
   * get the previous page of the search
   * @param {string} query
   * @param pagination
   * @returns {Observable<Object>}
   */
  getPrevious(query: string, pagination) {
    const prev = pagination.currentPage - 1;
    return this.http.get(url + `query=${query}:${pagination.sort}&currentPage=${prev}&pageSize=${pagination.pageSize}`);
  }

  /**
   * Loads an specific page
   * @param {number} page
   * @param {string} query
   * @param pagination
   * @returns {Observable<Object>}
   */
  goToPage(page: number, query: string, pagination) {
    return this.http.get(url + `query=${query}:${pagination.sort}&currentPage=${page}&pageSize=${pagination.pageSize}`);
  }
}
