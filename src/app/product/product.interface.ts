export interface Product {
  name: string;
  brand: string;
  description: string;
  url: string;
  code: string;
  price: {
    formattedValue: string
  };
}
