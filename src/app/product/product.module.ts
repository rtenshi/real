import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleProductComponent } from './single-product/single-product.component';
import {ProductService} from './product.service';
import {SearchComponent} from './search/search.component';
import {FormsModule} from '@angular/forms';
import { ListProductComponent } from './list-product/list-product.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    SingleProductComponent,
    SearchComponent,
    ListProductComponent
  ],
  declarations: [SingleProductComponent, SearchComponent, ListProductComponent],
  providers: [ProductService]
})
export class ProductModule { }
