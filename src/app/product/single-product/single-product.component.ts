import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../product.interface';
import {WishlistService} from '../../wishlist/wishlist.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.css']
})

export class SingleProductComponent implements OnInit {
  @Input() rawProduct: Product;
  @Output() updateList: EventEmitter<any> = new EventEmitter();

  public name: string;  // Product Name
  public brand: string; // Brand Name
  public price: any; // Price infos
  public description: string;
  public url: string;   // Product Single view
  public images: Array<any>;
  public isOnWishlist = false;

  constructor(private _store: WishlistService) {}

  ngOnInit() {
    this.setRaw();
    this.checkOnWishlist();
  }

  setRaw() {
    const def = {
      name: 'Product Name',
      brand: 'Product Brand', // Brand Name
      price: { // Price infos
        formattedValue: '€ 0.00'
      },
      images: [{url: '/blob.jpg'}],
      description: 'Product description',
      url: 'https://google.de'   // Product Single view
    };
    Object.assign(this, this, def, this.rawProduct);
  }

  /**
   * Checks if the product is on the wishlist
   */
  checkOnWishlist() {
    this.isOnWishlist = this._store.check(this.rawProduct);
  }

  /**
   * Add product to the wishlist
   */
  addToWishlist() {
    if (this._store.add(this.rawProduct)) {
      this.checkOnWishlist();
    }
  }

  /**
   * Remove product from the Wishlist
   */
  removeFromWishlist() {
    this._store.delete(this.rawProduct);
    this.checkOnWishlist();
    this.updateList.emit(null);
  }
}
