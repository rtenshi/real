import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {WishlistListComponent} from './wishlist/wishlist-list/wishlist-list.component';
import {SearchComponent} from './product/search/search.component';

const appRouters: Routes = [
  { path: '',                 component: SearchComponent},
  { path: 'wishlist',         component: WishlistListComponent},
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRouters)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
