import { browser, by, element, until } from 'protractor';

export class AppPage {
  navigateTo(target: string) {
    return browser.get(target);
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  search(keys: string) {
    return element(by.css('input')).sendKeys(keys);
  }



  public getSearchResultItems() {
    const condition = until.elementsLocated(by.css('.single-product'));
    return browser.wait(condition, 5000);
  }
}
