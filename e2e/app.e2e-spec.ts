import { AppPage } from './app.po';
import {browser, by, element, ElementArrayFinder} from 'protractor';

describe('real-digital App', () => {
  let page: AppPage;
  let products: ElementArrayFinder;

  beforeAll(() => {
    page = new AppPage();
    page.navigateTo('/');
  });

  it('should add a product to the wishlist', () => {
    page.search('pizza');
    browser.sleep(2000);
    products = element.all(by.css('.single-product'));
    expect(products.count()).toBeGreaterThan(0);
  });

  it('should add the lastfirst product to the wishlist', () => {
    element.all(by.css('.single-product button')).last().click();
    expect(element(by.buttonText('Remove item from Wishlist')).isPresent()).toBeTruthy();
  });

  it('should remove the last item of the wishlist', () => {
    element.all(by.css('.single-product button')).last().click();
    expect(element(by.buttonText('Remove item from Wishlist')).isPresent()).toBeFalsy();
  });

  it('should add the first product to the wishlist', () => {
    element.all(by.css('.single-product button')).first().click();
    expect(element(by.buttonText('Remove item from Wishlist')).isPresent()).toBeTruthy();
  });

  it('should load the wishlist', () => {
    page.navigateTo('/wishlist');
    browser.sleep(1000);
    expect(element(by.css('.wishlist-name > div')).isDisplayed).toBeTruthy();
  });

  it('can change the name of the list', () => {
    element(by.css('.wishlist-name > div')).click();
    browser.sleep(500);
    element(by.css('.wishlist-name .edit')).clear();
    element(by.css('.wishlist-name .edit')).sendKeys('My Pizza\'s Wishlist');
    browser.sleep(500);
    element(by.buttonText('Save')).click();
    browser.sleep(500);
    expect(element(by.css('.wishlist-name > div')).getText()).toContain('My Pizza\'s Wishlist');
  });

  it('item can be removed from the Wishlist', () => {
    element.all(by.buttonText('Remove item from Wishlist')).first().click();
    expect(element.all(by.css('.single-product')).count()).toEqual(0);
  });
});
