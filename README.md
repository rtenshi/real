# RealDigital



# Development

To test this project, you should add the following tags to the Chrome Broswer: 

##### Windows:
```
C:\Program Files (x86)\Google\Chrome\Application\chrome.exe  --disable-web-security --user-data-dir="C:\Chrome"
````

#### Linux
````angular2html
chrome-browser  --disable-web-security --user-data-dir
````

#### MacOs
````
open /Applications/Google\ Chrome.app --args --disable-web-security --user-data-dir
````

## Development server

Run `ng serve` or `npm run start`for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` or `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` or `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `npm run test-ci` to execute the unit tests using a headless Browser [PhantomJS](https://phantomjs.org).

## Running end-to-end tests

Run `ng e2e` or `npm run e2e`to execute the end-to-end tests via [Protractor](http://www.protractortest.org/). It will start a Chrome instance.
